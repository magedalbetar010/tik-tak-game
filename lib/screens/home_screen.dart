import 'package:flutter/material.dart';
import 'package:tik_tak/classes/logic_game.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String activePlayer = 'X';
  bool gameOver = false;
  int turn = 0;
  bool isSwitched = false;
  String result = "";
  OutomatickGame game = OutomatickGame();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme
          .of(context)
          .primaryColor,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SwitchListTile.adaptive(
                value: isSwitched,
                title: const Text(
                  "Turn on / of two player",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 35,
                  ),
                ),
                onChanged: (bool newValue) {
                  setState(
                        () {
                      isSwitched = newValue;
                    },
                  );
                },
              ),
              const SizedBox(
                height: 10,
              ),
              Center(
                child: Text(
                  "IT'S  $activePlayer TURN",
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w300,
                    fontSize: 56,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  decoration: const BoxDecoration(
                  ),
                  child: GridView.count(
                    padding: const EdgeInsets.all(12),
                    crossAxisSpacing: 6,
                    mainAxisSpacing: 6,
                    crossAxisCount: 3,

                    children: List.generate(
                      9,
                          (index) =>
                          InkWell(
                            onTap: () {
                              setState(() {
                                _onTap(index);
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: Theme
                                    .of(context)
                                    .shadowColor,
                              ),
                              child: Center(
                                child: Text(
                                    PlayerGame.playerX.contains(index)
                                        ? 'X'
                                        : PlayerGame.playerO.contains(index)
                                        ? 'O'
                                        : '',
                                    style: TextStyle(
                                      color: PlayerGame.playerX.contains(index)
                                          ? Colors.blue
                                          : Colors.pink,
                                      fontSize: 45,
                                    )),
                              ),
                            ),
                          ),
                    ),
                  ),
                ),
              ),
              Text(
                result,
                textAlign: TextAlign.center,
                style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w300,
                    fontSize: 42,
                    backgroundColor: Color(0xff4169e8)),
              ),
              ElevatedButton.icon(
                icon: const Icon(
                  Icons.replay,
                  color: Colors.white,
                ),
                label: const Text(
                  "Repate the game",
                  textAlign: TextAlign.center,
                ),
                onPressed: () {
                  setState(() {
                    repate();
                    // activePlayer = 'X';
                    // repate(activePlayer, gameOver, turn, result);

                  });
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  void repate() {
    {
      setState(() {
        activePlayer = "X";
        turn = 0;
        result = "";
        gameOver = false;
        PlayerGame.playerO = [];
        PlayerGame.playerX = [];
      });
    }
  }
  void repate2() {
    {
      setState(() {
        activePlayer = "X";
        turn = 0;
        gameOver = false;
        PlayerGame.playerO = [];
        PlayerGame.playerX = [];
      });
    }
  }

  _onTap(int index) async {
    if ((PlayerGame.playerX.isEmpty || !PlayerGame.playerX.contains(index)) &&
        (PlayerGame.playerO.isEmpty || !PlayerGame.playerO.contains(index))) {
      setState(() {
        Game().player(index, activePlayer);
        _updateState();
      });
      if (!isSwitched && !gameOver) {
        setState(() {
          game.outo(activePlayer);
          _updateState();
        });
      }
    }
  }

  void _updateState() {
    setState(() {
      activePlayer = activePlayer == 'X' ? 'O' : 'X';
      turn++;
      String win = checkWiner();
      if (win != "") {
        gameOver = true;
        result = "the winer is $win";
      }
      else if (turn == 9 ) {
        result = "It's Drow";
        gameOver = true;
      }
    }

  );
  }
}
