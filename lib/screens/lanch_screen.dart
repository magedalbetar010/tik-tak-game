import 'package:flutter/material.dart';

class LanchScreen extends StatefulWidget {
  const LanchScreen({Key? key}) : super(key: key);

  @override
  _LanchScreenState createState() => _LanchScreenState();
}

class _LanchScreenState extends State<LanchScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      Navigator.pushReplacementNamed(context, '/home_screen');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            'images/tiktak.png',
            // height: 700,
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),
           const Center(
            child: Text(
              "Are you ready for a challenge",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
          )
        ],
      ),
    );
  }
}
