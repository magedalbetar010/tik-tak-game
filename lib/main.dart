import 'package:flutter/material.dart';
import 'package:tik_tak/screens/lanch_screen.dart';
import 'package:tik_tak/screens/home_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
          brightness: Brightness.dark,
          primarySwatch: Colors.blue,
          primaryColor: const Color(0xff00061a),
          shadowColor: const Color(0xff001456),
          splashColor: const Color(0xff4169e8)),
      initialRoute: '/lanch_screen',
      routes: {
        '/lanch_screen': (context) => const LanchScreen(),
        '/home_screen': (context) => const HomeScreen(),
      },
    );
  }
}
